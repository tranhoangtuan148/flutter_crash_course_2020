import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  List list = ['Mặt trời', 'chân lý', 'chói qua tim', 'ahihi'];
  int myIndex = 0;

  void buttonTap(){
    myIndex++;
    print(myIndex);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text('My First Flutter App'),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(
                Icons.search,
                size: 40,
              ),
            ),
            MyText(myText: list[myIndex]),
            Center(
              child: MaterialButton(
                child: Text('Tap to next word'),
                onPressed: buttonTap,
                color: Colors.red,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyText extends StatelessWidget {
  final String myText;

  const MyText({Key key, this.myText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(
      myText,
      style: TextStyle(color: Colors.amber, fontSize: 40),
    ));
  }
}

//ctrl + alt + L
//alt + enter
